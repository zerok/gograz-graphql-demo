-- Write your migrate up statements here
CREATE TABLE locations (
    id uuid DEFAULT gen_random_uuid(),
    name varchar(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE events (
    id uuid DEFAULT gen_random_uuid(),
    starts_at timestamp with time zone NULL,
    ends_at timestamp with time zone NULL,
    title varchar(255) NOT NULL,
    location_id uuid NULL REFERENCES locations(id) ON DELETE SET NULL,
    PRIMARY KEY (id)
);

CREATE TABLE sessions (
    id uuid DEFAULT gen_random_uuid(),
    title varchar(255) NOT NULL,
    event_id uuid NULL REFERENCES events(id) ON DELETE SET NULL,
    PRIMARY KEY (id)
);

CREATE TABLE people (
    id uuid DEFAULT gen_random_uuid(),
    name varchar(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE session_presenters (
    id uuid DEFAULT gen_random_uuid(),
    session_id uuid NOT NULL REFERENCES sessions(id) ON DELETE CASCADE,
    person_id uuid NOT NULL REFERENCES people(id) ON DELETE CASCADE,
    PRIMARY KEY (id)
);

---- create above / drop below ----

DROP TABLE locations CASCADE;
DROP TABLE events CASCADE;
DROP TABLE people CASCADE;
DROP TABLE sessions CASCADE;
DROP TABLE session_presenters CASCADE;
