package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"gitlab.com/zerok/gograz-graphql/gograz"
	"gitlab.com/zerok/gograz-graphql/graph/generated"
	"gitlab.com/zerok/gograz-graphql/graph/model"
)

func (r *eventResolver) Location(ctx context.Context, obj *model.Event) (*model.Location, error) {
	loc, err := r.Index.Location(ctx, obj.Location.ID)
	if err != nil {
		return nil, err
	}
	return dbLocationToGQLocation(loc), nil
}

func (r *eventResolver) Sessions(ctx context.Context, obj *model.Event) ([]*model.Session, error) {
	sessions, err := r.Index.ListEventSessions(ctx, obj.ID)
	if err != nil {
		return nil, err
	}
	result := make([]*model.Session, 0, 10)
	for _, s := range sessions {
		result = append(result, &model.Session{
			ID:      gograz.UUIDToString(s.ID),
			Title:   s.Title,
			EventID: obj.ID,
		})
	}
	return result, nil
}

func (r *locationResolver) Events(ctx context.Context, obj *model.Location) ([]*model.Event, error) {
	events, err := r.Index.ListLocationEvents(ctx, obj.ID)
	if err != nil {
		return nil, err
	}
	return buildGQLEvents(events), nil
}

func (r *mutationResolver) CreateEvent(ctx context.Context, evt *model.CreateEvent) (*model.Event, error) {
	title := ""
	if evt.Title != nil {
		title = *evt.Title
	}
	evtID, err := r.Index.CreateEvent(ctx, gograz.Event{
		Title: title,
	})
	if err != nil {
		return nil, err
	}
	created, err := r.Index.Event(ctx, evtID)
	if err != nil {
		return nil, err
	}
	return &model.Event{
		ID:    gograz.UUIDToString(created.ID),
		Title: &created.Title,
	}, nil
}

func (r *mutationResolver) CreateSession(ctx context.Context, eventID string, title string) (*model.Session, error) {
	session, err := r.Index.CreateSession(ctx, eventID, title)
	if err != nil {
		return nil, err
	}
	return &model.Session{
		ID:    gograz.UUIDToString(session.ID),
		Title: session.Title,
	}, nil
}

func (r *personResolver) Sessions(ctx context.Context, obj *model.Person) ([]*model.Session, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) Event(ctx context.Context, id string) (*model.Event, error) {
	evt, err := r.Index.Event(ctx, id)
	if err != nil {
		return nil, err
	}
	return dbEventToGQEvent(evt), nil
}

func (r *queryResolver) Events(ctx context.Context, start *string, end *string) ([]*model.Event, error) {
	result := make([]*model.Event, 0, 10)
	events, err := r.Index.ListEvents(ctx)
	if err != nil {
		return nil, err
	}
	for _, e := range events {
		evt := dbEventToGQEvent(&e)
		result = append(result, evt)
	}
	return result, nil
}

func (r *queryResolver) Session(ctx context.Context, id string) (*model.Session, error) {
	session, err := r.Index.Session(ctx, id)
	if err != nil {
		return nil, err
	}
	return dbSessionToGQSession(session), nil
}

func (r *queryResolver) Locations(ctx context.Context) ([]*model.Location, error) {
	locations, err := r.Index.ListLocations(ctx)
	if err != nil {
		return nil, err
	}
	result := make([]*model.Location, 0, len(locations))
	for _, l := range locations {
		result = append(result, dbLocationToGQLocation(&l))
	}
	return result, nil
}

func (r *sessionResolver) Event(ctx context.Context, obj *model.Session) (*model.Event, error) {
	if obj.EventID == "" {
		return nil, nil
	}
	evt, err := r.Index.Event(ctx, obj.EventID)
	if err != nil {
		return nil, err
	}
	return dbEventToGQEvent(evt), nil
}

func (r *sessionResolver) Presenters(ctx context.Context, obj *model.Session) ([]*model.Person, error) {
	result := make([]*model.Person, 0, 10)
	people, err := r.Index.ListSessionPresenters(ctx, obj.ID)
	if err != nil {
		return nil, err
	}
	for _, p := range people {
		result = append(result, &model.Person{
			ID:   gograz.UUIDToString(p.ID),
			Name: p.Name,
		})
	}
	return result, nil
}

// Event returns generated.EventResolver implementation.
func (r *Resolver) Event() generated.EventResolver { return &eventResolver{r} }

// Location returns generated.LocationResolver implementation.
func (r *Resolver) Location() generated.LocationResolver { return &locationResolver{r} }

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Person returns generated.PersonResolver implementation.
func (r *Resolver) Person() generated.PersonResolver { return &personResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// Session returns generated.SessionResolver implementation.
func (r *Resolver) Session() generated.SessionResolver { return &sessionResolver{r} }

type eventResolver struct{ *Resolver }
type locationResolver struct{ *Resolver }
type mutationResolver struct{ *Resolver }
type personResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type sessionResolver struct{ *Resolver }
