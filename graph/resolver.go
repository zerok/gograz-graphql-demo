package graph

//go:generate go run github.com/99designs/gqlgen

import (
	"time"

	"github.com/jackc/pgtype"
	"gitlab.com/zerok/gograz-graphql/gograz"
	"gitlab.com/zerok/gograz-graphql/graph/model"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	Index *gograz.Index
}

func buildGQLEvents(input []gograz.Event) []*model.Event {
	result := make([]*model.Event, 0, len(input))
	for _, inp := range input {
		result = append(result, dbEventToGQEvent(&inp))
	}
	return result
}

func dbSessionToGQSession(input *gograz.Session) *model.Session {
	result := &model.Session{}
	result.ID = gograz.UUIDToString(input.ID)
	result.Title = input.Title
	result.EventID = gograz.UUIDToString(input.EventID)
	return result
}

func dbEventToGQEvent(input *gograz.Event) *model.Event {
	title := input.Title

	result := &model.Event{
		ID:    gograz.UUIDToString(input.ID),
		Title: &title,
	}
	if input.Start != nil {
		ts := input.Start.Format(time.RFC3339)
		result.Start = &ts
	}
	if input.End != nil {
		ts := input.End.Format(time.RFC3339)
		result.End = &ts
	}
	if input.LocationID.Status == pgtype.Present {
		result.Location = &model.Location{
			ID: gograz.UUIDToString(input.LocationID),
		}
	}
	return result
}

func dbLocationToGQLocation(input *gograz.Location) *model.Location {
	result := &model.Location{}
	result.ID = gograz.UUIDToString(input.ID)
	result.Name = input.Name
	return result
}
