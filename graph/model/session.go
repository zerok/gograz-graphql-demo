package model

type Session struct {
	ID    string `json:"id"`
	Title string `json:"title"`

	EventID string
}
