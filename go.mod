module gitlab.com/zerok/gograz-graphql

go 1.16

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/hashicorp/golang-lru v0.5.1 // indirect
	github.com/jackc/pgtype v1.8.1
	github.com/jackc/pgx/v4 v4.13.0
	github.com/machinebox/graphql v0.2.2
	github.com/matryer/is v1.4.0 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/rs/zerolog v1.15.0
	github.com/spf13/afero v1.6.0
	github.com/vektah/gqlparser/v2 v2.1.0
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
