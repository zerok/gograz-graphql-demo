DELETE FROM locations;
DELETE FROM events;

INSERT INTO locations (name) VALUES ('Zoom'), ('Lendplatz');

INSERT INTO events (title, starts_at, ends_at, location_id) VALUES 
('Juni 2021', '2021-06-21T19:00:00+02:00', '2021-06-21T21:00:00+02:00', (SELECT id FROM locations WHERE name = 'Zoom')),
('July 2021', '2021-07-19T19:00:00+02:00', '2021-07-19T21:00:00+02:00', (SELECT id FROM locations WHERE name = 'Lendplatz')),
('September 2021', '2021-09-20T19:00:00+02:00', '2021-09-20T21:00:00+02:00', (SELECT id FROM locations WHERE name = 'Zoom'));

INSERT INTO sessions (title, event_id) VALUES
('Introduction to GraphQL', (SELECT id FROM events WHERE title = 'September 2021'))
;
