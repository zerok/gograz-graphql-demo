package gograz

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/spf13/afero"
)

func UUIDToString(u pgtype.UUID) string {
	buf := make([]byte, 0, 36)
	out, err := u.EncodeText(nil, buf)
	if err != nil {
		fmt.Println(err.Error())
		return "error"
	}
	return string(out)
}

type Index struct {
	path string
	fs   afero.Fs
	conn *pgxpool.Pool
}

func NewIndex(conn *pgxpool.Pool) *Index {
	return &Index{
		conn: conn,
	}
}

type Event struct {
	ID         pgtype.UUID
	Title      string `toml:"title"`
	Start      *time.Time
	End        *time.Time
	LocationID pgtype.UUID
}

type Session struct {
	ID         pgtype.UUID
	Title      string   `toml:"title"`
	Presenters []string `toml:"presenters"`
	MeetupID   string   `toml:"meetupID"`
	EventID    pgtype.UUID
}

type Person struct {
	ID   pgtype.UUID
	Name string
}

type Location struct {
	ID   pgtype.UUID
	Name string
}

func (idx *Index) CreateEvent(ctx context.Context, evt Event) (string, error) {
	var id pgtype.UUID
	if err := idx.conn.QueryRow(ctx, "INSERT INTO events (title) VALUES ($1) RETURNING id", evt.Title).Scan(&id); err != nil {
		return "", err
	}
	return UUIDToString(id), nil
}

func (idx *Index) Event(ctx context.Context, id string) (*Event, error) {
	evt := Event{}
	var start pgtype.Timestamptz
	var end pgtype.Timestamptz
	if err := idx.conn.QueryRow(ctx, "SELECT id, title, starts_at, ends_at, location_id FROM events WHERE id = $1", id).Scan(&evt.ID, &evt.Title, &start, &end, &evt.LocationID); err != nil {
		return nil, err
	}
	if start.Status != pgtype.Null {
		evt.Start = &start.Time
	}
	if end.Status != pgtype.Null {
		evt.End = &end.Time
	}
	return &evt, nil
}

func (idx *Index) Session(ctx context.Context, id string) (*Session, error) {
	evt := Session{}
	if err := idx.conn.QueryRow(ctx, "SELECT id, title, event_id FROM sessions WHERE id = $1", id).Scan(&evt.ID, &evt.Title, &evt.EventID); err != nil {
		return nil, err
	}
	return &evt, nil
}

func (idx *Index) CreateSession(ctx context.Context, eventID string, title string) (*Session, error) {
	session := Session{}
	if err := idx.conn.QueryRow(ctx, "INSERT INTO sessions (event_id, title) VALUES ($1, $2) RETURNING id, event_id", eventID, title).Scan(&session.ID, &session.EventID); err != nil {
		return nil, err
	}
	session.Title = title
	return &session, nil
}

func (idx *Index) ListEvents(ctx context.Context) ([]Event, error) {
	result := make([]Event, 0, 10)
	rows, err := idx.conn.Query(ctx, "SELECT id, title, starts_at, ends_at, location_id FROM events ORDER BY starts_at DESC")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var start pgtype.Timestamptz
		var end pgtype.Timestamptz
		s := Event{}
		if err := rows.Scan(&s.ID, &s.Title, &start, &end, &s.LocationID); err != nil {
			return nil, err
		}
		if start.Status != pgtype.Null {
			s.Start = &start.Time
		}
		if end.Status != pgtype.Null {
			s.End = &end.Time
		}
		result = append(result, s)
	}
	return result, rows.Err()
}

func (idx *Index) ListSessionPresenters(ctx context.Context, id string) ([]Person, error) {
	result := make([]Person, 0, 10)
	rows, err := idx.conn.Query(ctx, "SELECT p.id, p.name FROM session_presenters sp LEFT JOIN people p ON (p.id = sp.person_id) WHERE sp.session_id = $1", id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		s := Person{}
		if err := rows.Scan(&s.ID, &s.Name); err != nil {
			return nil, err
		}
		result = append(result, s)
	}
	return result, nil
}

func (idx *Index) ListEventSessions(ctx context.Context, id string) ([]Session, error) {
	result := make([]Session, 0, 10)
	rows, err := idx.conn.Query(ctx, "SELECT id, title FROM sessions WHERE event_id = $1", id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		s := Session{}
		if err := rows.Scan(&s.ID, &s.Title); err != nil {
			return nil, err
		}
		result = append(result, s)
	}
	return result, nil
}

func (idx *Index) ListSessions(ctx context.Context) ([]Session, error) {
	result := make([]Session, 0, 10)
	rows, err := idx.conn.Query(ctx, "SELECT id, title, event_id FROM sessions")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		s := Session{}
		if err := rows.Scan(&s.ID, &s.Title, &s.MeetupID); err != nil {
			return nil, err
		}
		result = append(result, s)
	}
	return result, nil
}

func (idx *Index) ListLocations(ctx context.Context) ([]Location, error) {
	result := make([]Location, 0, 10)
	rows, err := idx.conn.Query(ctx, "SELECT id, name FROM locations")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		s := Location{}
		if err := rows.Scan(&s.ID, &s.Name); err != nil {
			return nil, err
		}
		result = append(result, s)
	}
	return result, nil
}

func (idx *Index) ListLocationEvents(ctx context.Context, locationID string) ([]Event, error) {
	result := make([]Event, 0, 10)
	rows, err := idx.conn.Query(ctx, "SELECT id, title, starts_at, ends_at, location_id FROM events WHERE location_id = $1 ORDER BY starts_at DESC", locationID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var start pgtype.Timestamptz
		var end pgtype.Timestamptz
		s := Event{}
		if err := rows.Scan(&s.ID, &s.Title, &start, &end, &s.LocationID); err != nil {
			return nil, err
		}
		if start.Status != pgtype.Null {
			s.Start = &start.Time
		}
		if end.Status != pgtype.Null {
			s.End = &end.Time
		}
		result = append(result, s)
	}
	return result, rows.Err()
}

func (idx *Index) Location(ctx context.Context, id string) (*Location, error) {
	var l Location
	if err := idx.conn.QueryRow(ctx, "SELECT id, name FROM locations WHERE id = $1", id).Scan(&l.ID, &l.Name); err != nil {
		return nil, err
	}
	return &l, nil
}
