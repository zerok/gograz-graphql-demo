# Demo for GoGraz: GraphQL

This repository contains the demo code and presentation I have during the
September 2021 event of the GoGraz usergroup.

## How to run

### 1. Start up the database server

```
docker-compose up
```

### 2. Update the database schema

The next step requires that you have [tern][] installed:

```
cd migrations
tern migrate
```

[tern]: https://github.com/jackc/tern

### 3. Add some sample data

```
psql -h localhost -U graphql < sample-data.sql
```

### 4. Start demo server

```
go run ./server.go
```

This will launch a demo server running on port 8080.
