package main

import (
	"context"
	"log"
	"net"
	"net/http"
	"os"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog"
	"gitlab.com/zerok/gograz-graphql/gograz"
	"gitlab.com/zerok/gograz-graphql/graph"
	"gitlab.com/zerok/gograz-graphql/graph/generated"
)

const defaultPort = "8080"

func main() {
	logger := zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr})
	ctx := logger.WithContext(context.Background())
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}
	conn, err := pgxpool.Connect(ctx, "postgres://graphql:graphql@localhost:5432/graphql")
	if err != nil {
		logger.Fatal().Err(err).Msg("Failed to connect to the database")
	}
	defer conn.Close()

	idx := gograz.NewIndex(conn)

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{Index: idx}}))
	httpSrv := http.Server{}

	mux := http.NewServeMux()
	mux.Handle("/query", srv)
	mux.Handle("/", playground.Handler("GraphQL playground", "/query"))

	httpSrv.Addr = "localhost:" + port
	httpSrv.Handler = mux
	httpSrv.BaseContext = func(l net.Listener) context.Context {
		return ctx
	}

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(httpSrv.ListenAndServe())
}
