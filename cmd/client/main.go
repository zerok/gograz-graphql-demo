package main

import (
	"context"
	"fmt"

	"github.com/machinebox/graphql"
	"gitlab.com/zerok/gograz-graphql/graph/model"
)

type Response struct {
	Meetups []model.Meetup `json: "meetups"`
}

func main() {
	client := graphql.NewClient("http://localhost:8080/query")
	req := graphql.NewRequest(`
query {
  meetups {
    id
  }
}
`)
	req.Var("$param", "hello")
	data := Response{}
	if err := client.Run(context.Background(), req, &data); err != nil {
		fmt.Println(err)
	}
	fmt.Println(data)
}
